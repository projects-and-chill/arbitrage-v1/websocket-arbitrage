import { WebSocket } from "ws"
import { comparaisonServiceState } from "./startComparationHandler"
import { ServerEvent, wsClients } from "./wss"

export const comparationStateHandler = (ws?: WebSocket) => {
  if (ws) {
    ws.send(JSON.stringify({ event: ServerEvent.comparationState, state: {
      started: comparaisonServiceState.started,
      launching: comparaisonServiceState.launching
    } }))
  }
  else {
    wsClients.forEach(ws => {
      ws.send(JSON.stringify({ event: ServerEvent.comparationState, state: {
        started: comparaisonServiceState.started,
        launching: comparaisonServiceState.launching
      } }))
    })
  }
}
