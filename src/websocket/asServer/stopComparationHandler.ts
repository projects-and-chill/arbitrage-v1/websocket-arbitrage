import { comparaisonServiceState } from "./startComparationHandler"
import { comparationStateHandler } from "./comparationStateHandler"
import { exchangeMetadataHandler } from "./exchangeMetadataHandler"
import chalk from "chalk"

export const stopComparationHandler = () => {
  console.log(chalk.red("STOP WEBSOCKET"))
  while (comparaisonServiceState.subjects.length)
    comparaisonServiceState.subjects.pop()?.complete()
  comparaisonServiceState.subscription.unsubscribe()
  comparaisonServiceState.launching = false
  comparaisonServiceState.started = false
  comparationStateHandler()
  exchangeMetadataHandler()
}
