import { WebSocket, WebSocketServer } from "ws"
import { comparaisonServiceState, startComparationHandler } from "./startComparationHandler"
import { stopComparationHandler } from "./stopComparationHandler"
import { comparationConfigHandler } from "./comparationConfigHandler"
import { getComparationConfig } from "../../services/getComparationConfig"
import { comparationStateHandler } from "./comparationStateHandler"
import { sleep } from "../../services/sleep"

enum ClientEvent {
  start = "start",
  stop = "stop",
  updateComparationConfig = "updateComparationConfig",
}

export enum ServerEvent {
  comparationConfig = "comparationConfig",
  comparationState = "comparationState",
  comparationExchanges = "comparationExchanges",
  add = "add",
  delete = "delete",
  update = "update",
  notification = "notification",
}

const {
  API_WS_NAME = "apiws",
  API_WS_HOST_PORT = "8081"
} = process.env

export const wsClients : Set<WebSocket> = new Set()

export const startWebSocketServer = () : WebSocketServer => {

  const wss = new WebSocketServer({ port: parseInt(API_WS_HOST_PORT), path: `/${API_WS_NAME}` })
  console.log(`Le path websocket est: ws://HOSTNAME/${API_WS_NAME}`)

  // Creating connection using websocket
  wss.on("connection", (ws: WebSocket) => {
    console.log("new client connected")
    wsClients.add(ws)
    
    //on message from client
    ws.on("message", async (dataBuffer) => {

      const rawData = dataBuffer.toString()
      if (!rawData)
        return
      const data = JSON.parse(rawData)
      if (!comparaisonServiceState.launching && comparaisonServiceState.started && data["event"] === ClientEvent.stop) {
        stopComparationHandler()
      }
      
      else if (!comparaisonServiceState.started && !comparaisonServiceState.launching && data["event"] === ClientEvent.start) {
        await startComparationHandler()
      }
      else if (data["event"] === ClientEvent.updateComparationConfig) {
        const wasStarted = comparaisonServiceState.started || comparaisonServiceState.launching
        if (wasStarted) {
          stopComparationHandler()
          comparaisonServiceState.launching = true
          comparationStateHandler()
          await sleep(4000)
        }
        await comparationConfigHandler(data["comparationConfig"])
        if (wasStarted)
          await startComparationHandler()
      }
    })

    ws.on("close", () => {
      wsClients.delete(ws)
      console.log("the client has disconnects from server")
    })
    // handling client connection error
    ws.onerror = function () {
      console.log("Some Error occurred")
    }

    getComparationConfig().then((comparationConfig)=>{
      ws.send(JSON.stringify({ event: ServerEvent.comparationConfig, comparationConfig }))
      comparationStateHandler(ws)
    })

  })
  console.log("The WebSocket server is running on port", API_WS_HOST_PORT)

  setInterval(()=> wsClients.forEach(ws => ws.ping()), 1800)
  return wss
}
