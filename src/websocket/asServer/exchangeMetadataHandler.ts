import { ServerEvent, wsClients } from "./wss"

export const exchangeMetadataHandler = (exchanges: string[] = [], exchangeCompatibilities: string[] = []) => {
  wsClients.forEach(wsClient => {
    const payload = { event: ServerEvent.comparationExchanges, exchanges, exchangeCompatibilities }
    wsClient.send(JSON.stringify(payload))
  })
}
