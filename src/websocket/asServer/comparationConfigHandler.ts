import { IComparationConfig } from "../../definition/IComparationConfig"
import { modelComparaisonConfig } from "../../models/model.comparationConfig"
import { ServerEvent, wsClients } from "./wss"

export const comparationConfigHandler = async (comparationConfig: IComparationConfig) => {
  await modelComparaisonConfig.findOneAndUpdate({}, comparationConfig)
  wsClients.forEach(ws => {
    ws.send(JSON.stringify({ event: ServerEvent.comparationConfig, comparationConfig }))
  })
}
