import { combineLatest, distinct, filter, map, Observable, Subject, Subscription } from "rxjs"
import { IAggragateOrderbook } from "../../definition/IAggragateOrderbook"
import crypto from "crypto"
import { aggrateOrderbook } from "../../services/aggrateOrderbook"
import { getOrderbookObservable } from "../asClient/cctxExchange"
import { buildCombination } from "../../services/buildCombinations"
import { IStopwatch, mesureEfficiencyTime } from "../../services/mesureEfficiencyTime"
import ccxt, { Exchange } from "ccxt"
import { bitrueExchangeId, getBitrueOrderbooks } from "../asClient/bitrue"
import { IOrderbook } from "../../definition/IOrderbook"
import { ICombination } from "../../definition/ICombination"
import { getComparationConfig } from "../../services/getComparationConfig"
import { ServerEvent, wsClients } from "./wss"
import { stopComparationHandler } from "./stopComparationHandler"
import { comparationStateHandler } from "./comparationStateHandler"
import { getExchangesMetadata } from "../../services/getExchangesMetadata"
import { exchangeMetadataHandler } from "./exchangeMetadataHandler"
import chalk from "chalk"
import { ExchangeIdType, mapExchangeId } from "../../services/exchangeMapping"
import { modelCredential } from "../../models/modelCredential"

interface IComparaisonService {
  launching: boolean
  started: boolean
  subjects: Subject<IOrderbook>[]
  subscription: Subscription
}

export const comparaisonServiceState : IComparaisonService = {
  launching: false,
  started: false,
  subjects: [],
  subscription: new Subscription()
}
//STOP

export const startComparationHandler = async () : Promise<void> => {

  if (comparaisonServiceState.subscription.closed)
    comparaisonServiceState.subscription = new Subscription()

  console.log(chalk.green("START WEBSOCKET"))
  comparaisonServiceState.launching = true
  comparationStateHandler()
  const { cctxPair, baseUsd, usdToSpend, minimumEfficiency } = await getComparationConfig()
  const { exchanges, exchangeCompatibilities } = await getExchangesMetadata(cctxPair)
  if (!exchanges || !exchangeCompatibilities){
    comparaisonServiceState.launching = false
    comparationStateHandler()
    wsClients.forEach(wsClient => {
      const payload = { event: ServerEvent.notification, notification: "No exchanges to compare !", type: "warning" }
      wsClient.send(JSON.stringify(payload))
    })
    return
  }
  exchangeMetadataHandler(exchanges, exchangeCompatibilities)

  const targetVolume = usdToSpend / baseUsd
  const observables : Observable<IAggragateOrderbook>[] = []
  const stopwatch : IStopwatch = new Map<string, ICombination>()

  /*----------------- SELFT CODED EXCHANGES ----------------*/
  if (exchanges.includes(bitrueExchangeId)) {
    const { ws, subject: bitrueSubject } = await getBitrueOrderbooks(cctxPair)
    comparaisonServiceState.subjects.push(bitrueSubject)
    bitrueSubject.subscribe({
      complete: ()=> ws.close()
    })
    
    observables.push(bitrueSubject.pipe(
      distinct(({ bids, asks }) => crypto.createHash("sha1").update(JSON.stringify({ bids, asks })).digest("base64")),
      map(orderbooks => aggrateOrderbook(orderbooks, targetVolume, cctxPair)))
    )
  }

  /*----------------- CCTX EXCHANGES ----------------*/
  const cctxObservables: Observable<IAggragateOrderbook>[] = (await getAvalaibleWSExchanges(exchanges))
    .map(exchange => getOrderbookObservable(exchange, cctxPair).pipe(
      distinct(({ bids, asks }) => crypto.createHash("sha1").update(JSON.stringify({ bids, asks })).digest("base64")),
      map(orderbook => aggrateOrderbook(orderbook, targetVolume, cctxPair))
    ))
  observables.push(...cctxObservables)

  /*----------------- GLOBAL OBSERVABLE ----------------*/

  if (observables.length < 2) {
    wsClients.forEach(wsClient => {
      const payload = { event: ServerEvent.notification, notification: "No exchanges to compare ! ", type: "warning" }
      wsClient.send(JSON.stringify(payload))
      stopComparationHandler()
    })
    return
  }

  const subscription : Subscription = combineLatest(observables)
    .pipe(
      filter(aggrateOrderbooks => !!aggrateOrderbooks.length),
      map(aggrateOrderbooks => buildCombination(aggrateOrderbooks, exchangeCompatibilities, minimumEfficiency))
    )
    .subscribe((combinations) => mesureEfficiencyTime(combinations, stopwatch, minimumEfficiency))
  comparaisonServiceState.subscription.add(subscription)

  comparaisonServiceState.started = true
  comparaisonServiceState.launching = false
  comparationStateHandler()

}

async function getAvalaibleWSExchanges (coinapiExchanges: string[]): Promise<string[]>{
  const exchanges: string[] = []
  const exchangeMappings = coinapiExchanges
    .map(coinapiExchangeId => ({
      coinapi: coinapiExchangeId,
      cctxWs: mapExchangeId(coinapiExchangeId, ExchangeIdType.COINAPI, ExchangeIdType.CCTX_WS)
    }))
  for (const exchangeMapping of exchangeMappings) {
    const exchangeCred : Record<string, any> | undefined = (await modelCredential.findOne({ market: exchangeMapping.coinapi }, "-market")) || {}

    const exchange: Exchange = new ccxt.pro[exchangeMapping.cctxWs](exchangeCred)
    if (exchange.has["watchOrderBook"])
      exchanges.push(exchangeMapping.cctxWs)
  }
  console.log("les exchanges websocket finaux", exchanges)
  return exchanges
}
