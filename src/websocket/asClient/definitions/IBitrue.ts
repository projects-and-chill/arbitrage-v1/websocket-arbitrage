export interface IBitrueOrderbook {
  channel: string,
  ts: number
  tick: {
    buys: Array<[price: string, volume: string]>
    asks: Array<[price: string, volume: string]>
  }
}