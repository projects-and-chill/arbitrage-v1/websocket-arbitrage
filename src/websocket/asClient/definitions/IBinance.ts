export interface IBinanceOrderbook {
  lastUpdateId: number,
  bids: Array<[price: string, volume: string]>
  asks: Array<[price: string, volume: string]>
}