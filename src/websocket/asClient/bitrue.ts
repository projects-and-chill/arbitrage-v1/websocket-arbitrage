import { WebSocket } from "ws"
import zlib from "zlib"
import { Subject } from "rxjs"
import { IBitrueOrderbook } from "./definitions/IBitrue"
import { DepthTuple, IOrderbook } from "../../definition/IOrderbook"
import chalk from "chalk"

const websocketUrl = "wss://ws.bitrue.com/market/ws"
export const bitrueExchangeId = "BITRUE" //Coinapi id

export const getBitrueOrderbooks = async (cctxPair: string) : Promise<{subject: Subject<IOrderbook>, ws: WebSocket}> => {

  const ws : WebSocket = await connectToServer()
  const subject = new Subject<IOrderbook>()
  const pair : string = formatPair(cctxPair)
  ws.send(JSON.stringify({
    "event": "sub",
    "params": {
      "cb_id": pair,
      "channel": `market_${pair}_simple_depth_step0`
    }
  }))

  ws.on("ping", ()=>{
    ws.pong()
  })

  ws.on("message", (rawData)=>{
    zlib.gunzip(rawData as any, (error, buffer)=> {
      const backendTimestamp = Date.now()
      const data : IBitrueOrderbook = JSON.parse(buffer.toString())
      if (data.tick) {
        subject.next({
          exchange: bitrueExchangeId,
          backendTimestamp,
          bids: data.tick.buys.map<DepthTuple>(([price, volume]) => [parseFloat(price), parseFloat(volume)]),
          asks: data.tick.asks.map<DepthTuple>(([price, volume]) => [parseFloat(price), parseFloat(volume)])
        })
      }
    })

  })
  return { ws, subject }
}

function formatPair (cctxPair: string) : string {
  return cctxPair.replace("/", "").toLowerCase()
}

async function connectToServer () : Promise<WebSocket> {
  console.log(chalk.cyan("ALONE", bitrueExchangeId))
  const ws = new WebSocket(websocketUrl)
  return new Promise((resolve) => {
    const timer = setInterval(() => {
      if (ws.readyState === 1) {
        console.log("Connexion established")
        clearInterval(timer)
        resolve(ws)
      }
    }, 10)
  })
}
