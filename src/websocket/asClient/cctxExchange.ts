import { IOrderbook } from "../../definition/IOrderbook"
import ccxt, { Exchange, version } from "ccxt"
import { ExchangeIdType, mapExchangeId } from "../../services/exchangeMapping"
import { fromAsyncIterable } from "rxjs/internal/observable/innerFrom"
import { Observable } from "rxjs"
import chalk from "chalk"

export const getOrderbookObservable = (exchangeCctxWs: string, pair: string) : Observable<IOrderbook> => {
  const generator : AsyncGenerator<IOrderbook> = orderbooksGenerator(exchangeCctxWs, pair)
  return fromAsyncIterable(generator)
}

export async function* orderbooksGenerator (exchangeCctxWs: string, pair: string): AsyncGenerator<IOrderbook> {

  console.log(chalk.magenta("CCTX ", exchangeCctxWs))

  const exchangeWS: Exchange = new ccxt.pro[exchangeCctxWs]()

  while (true as any) {
    const orderbook = await exchangeWS.watchOrderBook(pair)

    yield {
      exchange: mapExchangeId(exchangeCctxWs, ExchangeIdType.CCTX_WS, ExchangeIdType.COINAPI),
      backendTimestamp: Date.now(),
      asks: orderbook.asks,
      bids: orderbook.bids
    }
  }
}
