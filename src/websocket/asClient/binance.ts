import { WebSocket } from "ws"
import { Subject } from "rxjs"
import { IOrderbook } from "../../definition/IOrderbook"
import { IBinanceOrderbook } from "./definitions/IBinance"
import chalk from "chalk"

const websocketBaseurl = "wss://stream.binance.com:9443"
const exchangeID = "BINANCE" //Coinapi id

export const getBinanceSubject = async (cctxPair: string): Promise<Subject<IOrderbook>> => {
  const subject : Subject<IOrderbook> = new Subject()
  const ws: WebSocket = await connectToServer(cctxPair, 5)
  ws.send(JSON.stringify({
    method: "LIST_SUBSCRIPTIONS",
    id: 323
  }))

  ws.on("ping", () => {
    ws.pong()
  })

  ws.on("message", (rawData) => {
    const data : IBinanceOrderbook = JSON.parse(rawData.toString())
    subject.next({
      exchange: exchangeID,
      asks: data.asks.map(([price, volume]) => [parseFloat(price), parseFloat(volume)]),
      bids: data.bids.map(([price, volume]) => [parseFloat(price), parseFloat(volume)]),
      backendTimestamp: Date.now()
    })

  })
  return subject
}

async function connectToServer (cctxPair: string, levels: number): Promise<WebSocket> {
  console.log(chalk.cyan("ALONE", exchangeID))
  const pair = formatPair(cctxPair)
  const ws = new WebSocket(`${websocketBaseurl}/ws/${pair}@depth${levels}@100ms`)
  return new Promise((resolve) => {
    const timer = setInterval(() => {
      if (ws.readyState === 1) {
        console.log("Binance connexion established")
        clearInterval(timer)
        resolve(ws)
      }
    }, 10)
  })
}

function formatPair (cctxPair: string): string {
  return cctxPair.replace("/", "").toLowerCase()
}
