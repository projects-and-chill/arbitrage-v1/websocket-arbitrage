export interface ICombinationSide {
  symbol: string
  market: string
  price: number
  volume: number
}

export interface ICombination {
  id: string,
  name: string,
  pair: string,
  base: string,
  quote: string,
  efficiency: number
  timestamp: number
  buy: ICombinationSide,
  sell: ICombinationSide,
}
