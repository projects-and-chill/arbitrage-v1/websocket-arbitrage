export interface IMarketExclusion {
  markets: string[]
  severity: 0 | 1 | 2 | 3 | 4
  reasons: string[]
  note?: string
}