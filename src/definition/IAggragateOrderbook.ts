export interface IPriceAverage {
  price: number,
  volume: number,
  targetVolumeReached: boolean
}

export interface IAggragateOrderbook {
  exchange: string
  base: string
  quote: string
  backendTimestamp: number
  targetVolume: number
  sellAverage : IPriceAverage
  buyAverage : IPriceAverage
}