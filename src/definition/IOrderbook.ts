export type DepthTuple = [price: number, volume: number]

export interface IOrderbook {
  exchange: string
  backendTimestamp: number
  bids : Array<DepthTuple>
  asks : Array<DepthTuple>
}