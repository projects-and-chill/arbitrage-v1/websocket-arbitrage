import { model, Schema } from "mongoose"
import { INetwork } from "../definition/INetwork"

const schema = new Schema<INetwork>({
  name: { type: String, required: true },
  network: { type: String, required: true },
  asset: { type: String, required: true },
  market: { type: String, required: true },
  active: { type: Boolean, required: true },
  deposit: { type: Boolean, required: true },
  withdraw: { type: Boolean, required: true }
})

export const modelNetwork = model<INetwork>("networks", schema)
