import { startWebSocketServer } from "./websocket/asServer/wss"
import { dbConnexion } from "./mongodb"

const run = async () => {

  await dbConnexion()
  startWebSocketServer()
}

run()
  .then(() => console.log("\n\n----\"END OK----\n"))
  .catch((err) => console.log("\n\n---END BAD-----\n", err))
