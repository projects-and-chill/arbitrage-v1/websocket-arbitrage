import { connect } from "mongoose"

export async function dbConnexion (){
  const {
    MONGO_PORT = "27019",
    MONGO_DB = "arbitrage-db",
    MONGO_HOSTNAME = "localhost",
    MONGO_INITDB_USERNAME: user = "api",
    MONGO_INITDB_PASSWORD: pwd = "apipwd"
  } = process.env

  const uri = `mongodb://${user}:${pwd}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=${MONGO_DB}`
  console.log("Mongo URI is: ", uri)

  await connect(uri)
}
