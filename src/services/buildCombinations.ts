import { ICombination } from "../definition/ICombination"
import { IAggragateOrderbook } from "../definition/IAggragateOrderbook"
import { buildExchangeCombinationId } from "./getExchangesMetadata"

export const buildCombination = (
  aggregateOrderbooks: IAggragateOrderbook[],
  exchangeCompatibilities: string[],
  minimumSpread: number
): ICombination[] => {

  const len: number = aggregateOrderbooks.length
  const combinations: ICombination[] = []
  const orderbooksCopy: IAggragateOrderbook[] = [...aggregateOrderbooks]
  for (let i = 0; i < len; i++) {
    const currOrderbook: IAggragateOrderbook = orderbooksCopy.pop() as IAggragateOrderbook
    combinations.push(...buyCombinations(currOrderbook, orderbooksCopy, exchangeCompatibilities, minimumSpread))
    combinations.push(...sellCombinations(currOrderbook, orderbooksCopy, exchangeCompatibilities, minimumSpread))
  }

  return combinations.sort((a, b) => a.efficiency - b.efficiency)
}

function buyCombinations (
  currentOrderbook: IAggragateOrderbook,
  otherOrderbooks: IAggragateOrderbook[],
  exchangeCompatibilities: string [],
  minimumSpread: number
): ICombination[] {

  let spread: number
  const buyEligible = otherOrderbooks.filter(orderbook =>
    currentOrderbook.buyAverage.targetVolumeReached &&
    orderbook.sellAverage.targetVolumeReached &&
    exchangeCompatibilities.includes(buildExchangeCombinationId(currentOrderbook.exchange, orderbook.exchange)) &&
    currentOrderbook.buyAverage.price < orderbook.sellAverage.price &&
    (spread = getSpread(currentOrderbook.buyAverage.price, orderbook.sellAverage.price)) >= minimumSpread
  )

  return buyEligible.map((orderbook): ICombination => {
    const { exchange: buyExchange, base, quote } = currentOrderbook
    const { exchange: sellExchange } = orderbook

    const combinationName = getCombinationName(buyExchange, sellExchange, base, quote)
    const combinationTimestamp = orderbook.backendTimestamp > currentOrderbook.backendTimestamp
      ? orderbook.backendTimestamp
      : currentOrderbook.backendTimestamp

    return {
      id: `${combinationName}-${combinationTimestamp}`,
      name: combinationName,
      efficiency: spread,
      timestamp: combinationTimestamp,
      base,
      quote,
      pair: `${base}_${quote}`,
      buy: {
        market: buyExchange,
        symbol: `${buyExchange}_${base}_${quote}`,
        price: currentOrderbook.buyAverage.price,
        volume: currentOrderbook.buyAverage.volume
      },
      sell: {
        market: sellExchange,
        symbol: `${sellExchange}_${base}_${quote}`,
        price: orderbook.sellAverage.price,
        volume: orderbook.sellAverage.volume
      }
    }

  })
}

function sellCombinations (
  currentOrderbook: IAggragateOrderbook,
  otherOrderbooks: IAggragateOrderbook[],
  exchangeCompatibilities: string[],
  minimumSpread: number
): ICombination[] {

  let spread: number
  const sellEligible = otherOrderbooks.filter(orderbook =>
    orderbook.buyAverage.targetVolumeReached &&
    currentOrderbook.sellAverage.targetVolumeReached &&
    exchangeCompatibilities.includes(buildExchangeCombinationId(orderbook.exchange, currentOrderbook.exchange)) &&
    orderbook.buyAverage.price < currentOrderbook.sellAverage.price &&
    (spread = getSpread(orderbook.buyAverage.price, currentOrderbook.sellAverage.price)) >= minimumSpread
  )

  return sellEligible.map((orderbook): ICombination => {
    const { exchange: buyExchange, base, quote } = orderbook
    const { exchange: sellExchange } = currentOrderbook

    const combinationName = getCombinationName(buyExchange, sellExchange, base, quote)
    const combinationTimestamp = orderbook.backendTimestamp > currentOrderbook.backendTimestamp
      ? orderbook.backendTimestamp
      : currentOrderbook.backendTimestamp

    return {
      id: `${combinationName}-${combinationTimestamp}`,
      name: combinationName,
      efficiency: spread,
      timestamp: combinationTimestamp,
      base,
      quote,
      pair: `${base}_${quote}`,
      buy: {
        market: buyExchange,
        symbol: `${buyExchange}_${base}_${quote}`,
        price: orderbook.buyAverage.price,
        volume: orderbook.buyAverage.volume
      },
      sell: {
        market: sellExchange,
        symbol: `${sellExchange}_${base}_${quote}`,
        price: currentOrderbook.sellAverage.price,
        volume: currentOrderbook.sellAverage.volume
      }
    }

  })
}

function getSpread (buyPrice: number, sellPrice: number): number {

  const mainNumber: number = buyPrice
  const subNumber: number = sellPrice - buyPrice
  const percentage = subNumber / mainNumber * 100
  return Math.round((percentage + Number.EPSILON) * 100) / 100
}

function getCombinationName (buyMarket, sellMarket, base, quote) {
  return `${buyMarket}_${sellMarket}_${base}_${quote}`
}
