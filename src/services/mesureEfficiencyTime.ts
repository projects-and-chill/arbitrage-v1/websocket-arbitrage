import chalk from "chalk"
import { ICombination } from "../definition/ICombination"
import { wsClients } from "../websocket/asServer/wss"

export type IStopwatch = Map<string, ICombination>

enum action {
  delete = "delete",
  add = "add",
  update = "update",
}

export const mesureEfficiencyTime = async (
  combinations : ICombination[],
  combinationStopwatch: IStopwatch,
  minimumEfficiency: number) => {

  console.log("Results:", combinations)
  const now : number = Date.now()

  for (const [combinationName, prevCombination] of combinationStopwatch.entries()){
    const stillProfitable : boolean = combinations.some(combination => combination.name === combinationName)
    const elapsedTimeInSeconds = (now - prevCombination.timestamp) / 1000
    if (!stillProfitable) {
      combinationStopwatch.delete(combinationName)
      // eslint-disable-next-line max-len
      console.log(chalk.yellow(`The "${chalk.magenta(combinationName)}" combination was profitable >= at ${chalk.magenta(minimumEfficiency + "%")} during: ${chalk.hex(colorPicker(elapsedTimeInSeconds)).bgGrey(elapsedTimeInSeconds)} seconds`))
      const payload : string = JSON.stringify({
        event: action.delete,
        combinationName,
        targetEfficienty: minimumEfficiency,
        backendTimestamp: now
      })
      wsClients.forEach(wsClient => wsClient.send(payload))
    }

  }

  combinations.forEach(combination => {
    const combinationExist = combinationStopwatch.has(combination.name)
    if (!combinationExist) {
      const payload : string = JSON.stringify({
        event: action.add,
        combination,
        backendTimestamp: now
      })
      wsClients.forEach(wsClient => wsClient.send(payload))
      combinationStopwatch.set(combination.name, combination)
    }
    else if (combinationExist && combinationStopwatch.get(combination.name)?.efficiency !== combination.efficiency) {
      const payload : string = JSON.stringify({
        event: action.update,
        efficiency: combination.efficiency,
        combinationName: combination.name,
        combinationTimestamp: combination.timestamp,
        backendTimestamp: now
      })
      wsClients.forEach(wsClient => wsClient.send(payload))
      combinationStopwatch.set(combination.name, combination)
    }

  })
}

function colorPicker (seconds: number) : string {
  if (seconds < 1)
    return "#ff0000"//"red"
  else if (seconds < 2.5)
    return "#ff7d00"//"orange"
  else if (seconds < 4.5)
    return "#ffdd00"//"yellow"
  else
    return "#1eff00"//"green"
}
