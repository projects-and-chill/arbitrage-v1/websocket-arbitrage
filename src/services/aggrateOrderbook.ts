import { IAggragateOrderbook, IPriceAverage } from "../definition/IAggragateOrderbook"
import { DepthTuple, IOrderbook } from "../definition/IOrderbook"

export const aggrateOrderbook =
  (orderbook:IOrderbook, targetVolume:number, cctxPair: string) : IAggragateOrderbook =>{
    const { exchange, bids, asks, backendTimestamp } = orderbook
    const [base, quote] = cctxPair.split("/")
    return {
      exchange,
      base,
      quote,
      backendTimestamp,
      targetVolume,
      sellAverage: aggregateSide(bids, targetVolume),
      buyAverage: aggregateSide(asks, targetVolume)
    }
  }

function aggregateSide (depthTuples : DepthTuple[], targetVolume : number) : IPriceAverage {
  let totalCost = 0
  let currVolume = 0
  for (const [price, volume] of depthTuples) {

    if (currVolume + volume <= targetVolume){
      currVolume += volume
      totalCost += price * volume
    }
    else if (currVolume + volume > targetVolume) {
      const usedVolume = targetVolume - currVolume
      currVolume += usedVolume
      totalCost += price * usedVolume
    }
    if (currVolume === targetVolume)
      break
  }

  return {
    price: totalCost / currVolume,
    volume: currVolume,
    targetVolumeReached: currVolume === targetVolume
  }

}