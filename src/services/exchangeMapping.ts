import exchangeMapping from "../exchangesMapping.json"

type IExchangeData = typeof exchangeMapping[number]

export enum ExchangeIdType {
  CCTX_WS = "cctx_ws",
  CCTX_REST = "cctx_rest",
  COINAPI = "coinapi",
}

export const mapExchangeId = (initialId: string, initialType: ExchangeIdType, targetType: ExchangeIdType) : string => {
  const exchangeData : IExchangeData | undefined = exchangeMapping.find(exchange => exchange[initialType] === initialId)
  if (!exchangeData)
    throw "INVALID MAPPING FILE !"
  return exchangeData[targetType]
}
