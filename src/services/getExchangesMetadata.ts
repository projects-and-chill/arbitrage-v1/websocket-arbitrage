import modelSymbol from "../models/model.symbol"
import { INetwork } from "../definition/INetwork"
import { modelNetwork } from "../models/model.network"

interface IExchangesMetadata { exchanges: string[], exchangeCompatibilities: string[] }

const getExchangesMetadata = async (cctxPair: string) : Promise<IExchangesMetadata> => {
  const pair = pairCctxToCoinapi(cctxPair)
  const mongoRequest = [
    { $match: { "exclusion.isExclude": false, pair } },
    { $lookup: {
      from: "pairs",
      localField: "pair",
      foreignField: "name",
      as: "pair"
    } },
    { $lookup: {
      from: "markets",
      localField: "market",
      foreignField: "name",
      as: "market"
    } },
    { $lookup: {
      from: "assets",
      localField: "base",
      foreignField: "name",
      as: "base"
    } },
    { $lookup: {
      from: "assets",
      localField: "quote",
      foreignField: "name",
      as: "quote"
    } },
    { $unwind: "$pair" },
    { $unwind: "$market" },
    { $unwind: "$base" },
    { $unwind: "$quote" },
    { $match: {
      "market.exclusion.isExclude": false,
      "pair.exclusion.isExclude": false,
      "base.exclusion.isExclude": false,
      "quote.exclusion.isExclude": false
    } },
    { $group: { _id: "$market.name" } }
  ]

  const exchanges : string[] = (await modelSymbol.aggregate(mongoRequest))?.map(result => result._id) || []
  if (!exchanges)
    return { exchanges, exchangeCompatibilities: [] }

  return await getNetworksCompatibility(cctxPair, exchanges)
}

/**
 *
 * Attention il y a de fortes chances que le filtrage par networksIDs exclus des combinaisons valables !
 *
 * En effet, certains networkIDs peuvent avoir une nomenclature différentes selon les markets. Par exemple: "ERC20" vs "ETH".
 * Dans ces situations, l'algorithme manquera de créer ces combinaisons dont la nomemclature des networkIDs est différente.
 *
 * --> Pour régulariser celà il faudrai normaliser la nomemclature de chaque network avant de les enregistrer en DB.
 * */
const getNetworksCompatibility = async (cctxPair: string, markets: string[]) : Promise<IExchangesMetadata> =>{
  if (!markets)
    return { exchanges: [], exchangeCompatibilities: [] }
  const baseAsset = cctxPair.split("/")[0]
  const networks : INetwork[] = await modelNetwork.find({ asset: baseAsset, market: { $in: markets } }).lean()
  const networksLen : number = networks.length
  const exchanges : Set<string> = new Set()
  const exchangeCompatibilities : Set<string> = new Set()
  for (let i = 0; i < networksLen; i++){
    const currentNetworkData = networks.pop() as INetwork
    networks.forEach(networkData =>{
      const [currNetworkId, networkId] = [currentNetworkData.network, networkData.network]
      if (
        currentNetworkData.market === networkData.market
        || !currNetworkId.includes(networkId) && !networkId.includes(currNetworkId)
      )
        return

      if (currentNetworkData.withdraw && networkData.deposit) {
        exchangeCompatibilities.add(buildExchangeCombinationId(currentNetworkData.market, networkData.market))
        exchanges.add(currentNetworkData.market).add(networkData.market)
      }

      if (networkData.withdraw && currentNetworkData.deposit) {
        exchangeCompatibilities.add(buildExchangeCombinationId(networkData.market, currentNetworkData.market))
        exchanges.add(currentNetworkData.market).add(networkData.market)
      }

    })
  }
  return { exchanges: [...exchanges], exchangeCompatibilities: [...exchangeCompatibilities] }
}

const pairCctxToCoinapi = (cctxPair: string) : string => {
  return cctxPair.toUpperCase().replace("/", "_")
}

const buildExchangeCombinationId = (buyMarket: string, sellMarket: string) : string => `${buyMarket}_${sellMarket}`

export { buildExchangeCombinationId, getExchangesMetadata, IExchangesMetadata }
