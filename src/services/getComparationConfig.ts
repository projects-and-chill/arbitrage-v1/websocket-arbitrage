import { IComparationConfig } from "../definition/IComparationConfig"
import { modelComparaisonConfig } from "../models/model.comparationConfig"
import config from "../defaultConfiguration.json"

export const getComparationConfig = async () : Promise<IComparationConfig> => {
  const comparationConfig : IComparationConfig | null = await modelComparaisonConfig.findOne({}).lean()
  if (comparationConfig)
    return comparationConfig

  const defaultComparationConfig : IComparationConfig = {
    cctxPair: config.cctx_pair,
    usdToSpend: config.usd_to_spend,
    baseUsd: config.base_asset_to_usd,
    minimumEfficiency: config.minimum_efficiency_expected
  }
  await new modelComparaisonConfig(defaultComparationConfig).save()
  
  return defaultComparationConfig
}
